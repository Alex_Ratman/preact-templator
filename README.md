# Preact templator #

Preact to HTML template script (SSR)

### Options ###

.templatorrc config options

* **input** *{Object} [required] Options: **data** - initial state data dir, **js** - components dir, **html** - templates dir* - data entry point
* **output** *{String} [required]* - entry point for Preact components to be placed in HTML template (file or directory)
* **templates** *{Array}* - list of pages to be templated
* **selectors** *{Object} Default: app = '@{app}', state = '@{state}'* - patterns to be replaced

### Template data structure ###

* **name** *{String}* - output template name
* **html** *{String} [required]* - input template file name
* **component** *{String} [required]* - components file name
* **js** *{String}* - js scripts file name
* **css** *{String}* - css scripts file name
* **data** *{String}* - data file name (initial state)
* **description** *{String}* - output html meta description
* **title** *{String}* - output html meta title

### Example ###

**
```html
<!-- src/templates/business.html -->

<!DOCTYPE html>
<html lang="en">
  <head>
  	<meta name"description" content="@{description}"/>
	<title>@{title}</title>
	<link rel="preload" as="style" href="public/css/@{styles}"/>
    <link rel="stylesheet" href="public/css/@{styles}"/>
  </head>
  <body>
    <div id="app">@{app}</div>
    <script type="module" src="js/views/@{scripts}.js"></script>
	<script>window.__APP_INITIAL_STATE__ = @{state}</script>
  </body>
</html>
```

```javascript
// src/js/components/index.js

import { h } from '../web_modules/preact.js';

function Business(props) {
  return <h1>My component</h1>;
}

export {
  Business
};
```

```javascript
// .templatorrc

{
...
{
  "input": {
    "data": "server/assets/data",
    "js": "public/js/main",
    "html": "client/html"
  },
  "output": "public",
  "templates": [
    {
      "name": "business",
      "html": "business.html",
      "component": "Business.js",
      "js": "business.js",
      "css": "business.css",
      "data": "business.json",
      "description": "Some page description.",
      "title": "My best page"
    }
  ]
}
...
}
```

```javascript
// business.json - initial state

{ 
  "products": [
    {
      "brand": "MY BRAND",
      "id": "XYZ",
      "href": "https://google.com",
      "name": "My perfect data"
    }
  ]
  ...
}
```