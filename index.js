const fs = require('fs');
const path = require('path');
const util = require('util');
const { h } = require('preact');
const render = require('preact-render-to-string');

const mkdir = util.promisify(fs.mkdir);
const readdir = util.promisify(fs.readdir);
const readfile = util.promisify(fs.readFile);
const writefile = util.promisify(fs.writeFile);

const fillTemplate = async ({ entrypoint, paths, selectors, template }) => {
  const {
    css,
    component,
    data,
    description,
    html,
    js,
    name,
    title
  } = template;
  // Get data
  let dataContent;
  if (data) {
    const dataFile = await readfile(path.resolve(entrypoint, data), 'utf8');
    dataContent = JSON.parse(dataFile);
  }
  // Get HTML template
  let htmlFile = await readfile(`${paths.html}/${html}`, 'utf8');
  // Get Preact component content
  const componentFile = await import(`${paths.js}/${component}`);
  let componentExport = componentFile[component.split('.')[0].split('/').pop()] || componentFile.default;
  if (!componentExport) throw new Error(`Templator was unable to create template for "${paths.js}/${component}" - export not found`);
  const componentContent = render(h(componentExport, {...dataContent }));

  // Fill template
  const stylesPattern = new RegExp(selectors.styles, 'g');
  htmlFile = htmlFile
    .replace(selectors.app, componentContent)
    .replace(selectors.state, JSON.stringify(dataContent))
    .replace(selectors.description, description)
    .replace(selectors.title, title)
    .replace(selectors.scripts, js)
    .replace(stylesPattern, css)

  // Write template
  let outputPath = paths.output;
  const splittedName = name.split('/');
  if (splittedName.length > 1) { outputPath = `${paths.output}/${splittedName.slice(0,-1).join('/')}` }
  if (!fs.existsSync(outputPath)) { await mkdir(outputPath, { recursive: true }) };
  await writefile(`${outputPath}/${splittedName.pop()}.html`, htmlFile, 'utf8');
}

const templator = async () => {
  try {
    const config = await readfile(path.resolve(process.cwd(), '.templatorrc'), 'utf8');
    const {
      input = {},
      output,
      templates,
      selectors = {
        app: '@{app}',
        description: '@{description}',
        title: '@{title}',
        scripts: '@{scripts}',
        state: '@{state}',
        styles: '@{styles}'
      }
    } = JSON.parse(config);
    const paths = {
      js: path.resolve(process.cwd(), input.js),
      html: path.resolve(process.cwd(), input.html),
      output: path.resolve(process.cwd(), output)
    };

    for (let entrypoint of templates) {
      if (typeof entrypoint !== 'string') {
        throw new Error('Template path should be a type of string');
      }

      entrypoint = path.resolve(process.cwd(), entrypoint);

      if (!fs.existsSync(entrypoint)) {
        throw new Error(`Template entrypoint ${entrypoint} does not exist`);
      }

      if (fs.statSync(entrypoint).isDirectory()) {
        const files = await readdir(entrypoint);
        for (let file of files) {
          file = path.resolve(entrypoint, file);
          const template = JSON.parse(await readfile(file, 'utf8'));
          fillTemplate({
            entrypoint,
            paths,
            selectors,
            template
          });
        }
      } else {
        const template = JSON.parse(await readfile(entrypoint, 'utf8'));
        fillTemplate({
          entrypoint,
          paths,
          selectors,
          template
        });
      }
    }
  } catch (e) {
    console.error(e);
  }
};

module.exports = templator;